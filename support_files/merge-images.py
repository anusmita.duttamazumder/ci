import argparse
import shlex
import subprocess
import sys
import re

sector_size = 512


class Partition:
    image: str
    number: int
    start: int
    end: int
    type_code: str
    uuid: str
    guid: str
    name: str

    @property
    def size(self):
        return self.end - self.start + 1

    def __repr__(self):
        return f"Partition({self.image}/{self.name} {self.number},{self.start}-{self.end},{self.type_code})"


def run(cmd):
    print(shlex.join(cmd))
    subprocess.check_call(cmd)


def read_partitions(filename):
    lines = subprocess.check_output(
        ["sgdisk", "--print", filename], encoding="utf-8"
    ).splitlines()
    lines = [line.split() for line in lines]
    while lines[0][0:1] != ["Number"]:
        lines.pop(0)
    lines = [line for line in lines if len(line) > 0 and re.match(r"^[0-9]+$", line[0])]
    partitions = []
    for line in lines:
        partition = Partition()
        partition.image = filename
        partition.number = int(line.pop(0))
        partition.start = int(line.pop(0))
        partition.end = int(line.pop(0))
        partition.guid = None
        partition.uuid = None
        partinfo = subprocess.check_output(["sgdisk", "-i", f"{partition.number}", filename]).splitlines()
        for pline in partinfo:
            if pline.startswith(b"Partition GUID code:"):
                partition.guid = pline.split()[3].decode('utf8')
            elif pline.startswith(b"Partition unique GUID:"):
                partition.uuid = pline.split()[3].decode('utf8')
        line.pop(0)
        line.pop(0)  # discard size
        partition.type_code = line.pop(0)
        partition.name = " ".join(line)
        partitions.append(partition)
    return partitions


def create_dest_image(output, sources, hybrid):
    n = 0
    size_in_sectors = sum([p.size for p in sources])
    size_in_bytes = sector_size * size_in_sectors + (10 * 1024 * 1024)  # 10MB extra
    run(["truncate", f"--size={size_in_bytes}", output])
    cmd = ["sgdisk"]
    n = 0
    for part in sources:
        n += 1
        cmd += [
            f"--new={n}:0:+{part.size}",
            f"--change-name={n}:{part.name}",
        ]
        if part.guid:
            cmd += [f"--typecode={n}:{part.guid}"]
        else:
            cmd += [f"--typecode={n}:{part.type_code}"]
        if part.uuid:
            cmd += [f"--partition-guid={n}:{part.uuid}"]
    if hybrid:
        cmd.append(f"--hybrid={hybrid}")
    cmd.append(output)
    run(cmd)


def copy_partitions(source, dest):
    for source_part, dest_part in zip(source, dest):
        if source_part.size != dest_part.size:
            raise RuntimeError(f"{source_part} and {dest_part} have different sizes!")
        run(
            [
                "dd",
                f"bs={sector_size}",
                "conv=notrunc",
                f"skip={source_part.start}",
                f"seek={dest_part.start}",
                f"count={source_part.size}",
                f"if={source_part.image}",
                f"of={dest_part.image}",
            ]
        )


def main():
    parser = argparse.ArgumentParser(prog=sys.argv[0], add_help=False)
    parser.add_argument(
        "-o", "--output", type=str, help="output image filename", default="output.img"
    )
    parser.add_argument(
        "-h",
        "--hybrid",
        type=str,
        help="create a hybrid MBR, as in sgdisk(1)",
        default=None,
    )
    parser.add_argument("--help", action="store_true")
    parser.add_argument("images", nargs="+", metavar="IMAGE")

    opts = parser.parse_args(sys.argv[1:])

    if opts.help:
        parser.print_help()
        sys.exit(0)

    sources = []
    for f in opts.images:
        sources += read_partitions(f)
    for p in sources:
        print(p)
    output = opts.output
    run(["rm", "-f", output])
    create_dest_image(output, sources, opts.hybrid)
    dest = read_partitions(output)
    for p in dest:
        print(p)
    copy_partitions(sources, dest)


if __name__ == "__main__":
    main()
